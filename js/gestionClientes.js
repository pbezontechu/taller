var customersString;

function getCustomers() {
    //var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Customers?$filter=Country eq 'Germany'";
var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Customers";
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
      if (this.readyState==4 && this.status == 200) {
        console.log(request.responseText);
        customersString = request.responseText;
        processCustomers();
      }
    }
    request.open("GET", url, true);
    request.send();
}

var flag = "https://upload.wikimedia.org/wikipedia/en/thumb/b/ba/Flag_of_"
var flag2 = ".svg/100px-Flag_of_";
var flag3 = ".svg.png";

var flagReal = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
var flagReal2 = ".png"


function processCustomers() {
  var customersJson = JSON.parse(customersString);
  var table = document.getElementById("customersTable");
  for (customer of customersJson.value) {
    var newRow = document.createElement("tr");
    var nameColumn =  document.createElement("td");
    nameColumn.innerText = customer.ContactName;
    var cityColumn =  document.createElement("td");
    cityColumn.innerText = customer.City;
    var imageColumn =  document.createElement("IMG");

    //imageColumn.src = flag+customer.Country+flag2+customer.Country+flag3;
    if (customer.Country == "UK") {
      imageColumn.src = flagReal+"United-Kingdom"+flagReal2;
    } else {
      imageColumn.src = flagReal+customer.Country+flagReal2;
    }

    imageColumn.classList.add("flag");

    newRow.appendChild(nameColumn);
    newRow.appendChild(cityColumn);
    newRow.appendChild(imageColumn);

    table.appendChild(newRow);
  }

  table.class = "table table-striped";
}
