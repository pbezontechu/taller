var productsString;
function getProducts() {
    var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Products";
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
      if (this.readyState==4 && this.status == 200) {
        console.log(request.responseText);
        productsString = request.responseText;
        processProducts();
      }
    }
    request.open("GET", url, true);
    request.send();
}

function processProducts() {

  var productsJson = JSON.parse(productsString);
  var table = document.getElementById("productsTable");
  for (product of productsJson.value) {
    var newRow = document.createElement("tr");
    var nameColumn =  document.createElement("td");
    nameColumn.innerText = product.ProductName;
    var priceColumn =  document.createElement("td");
    priceColumn.innerText = product.UnitPrice;
    var stockColumn =  document.createElement("td");
    stockColumn .innerText = product.UnitsInStock;

    newRow.appendChild(nameColumn);
    newRow.appendChild(priceColumn);
    newRow.appendChild(stockColumn);

    table.appendChild(newRow);
  }
}
